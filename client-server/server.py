import numpy as np
import urllib.request
import json
import cv2
import os

from flask import Flask, jsonify, request

app = Flask(__name__)



def _grab_image(path=None, stream=None, url=None):
	# if the path is not None, then load the image from disk
	if path is not None:
		image = cv2.imread(path)
	# otherwise, the image does not reside on disk
	else:	
		# if the URL is not None, then download the image
		if url is not None:
			resp = urllib.request.urlopen(url)
			data = resp.read()
		# if the stream is not None, then the image has been uploaded
		elif stream is not None:
			data = stream.read()
		# convert the image to a NumPy array and then read it into
		# OpenCV format
		image = np.asarray(bytearray(data), dtype="uint8")
		image = cv2.imdecode(image, cv2.IMREAD_COLOR)
	# return the image
	return image

    
@app.route("/",methods = ['POST'])
###Серце обробки###
def process():
	# initialize the data dictionary to be returned by the request
	data = {"success": False}
	# check to see if this is a post request
		# check to see if an image was uploaded
	if request.files.get("image", None) is not None:
			# grab the uploaded image
	    image = _grab_image(stream=request.files["image"])
		# otherwise, assume that a URL was passed in
	else:
	    data["error"] = "No URL provided."
	    return jsonify(data)
		# convert the image to grayscale, load the face cascade detector,
		# and detect faces in the image
	###Те шо зверху це по факту аналог
        #while True:
        #(grabbed, frame) = vs.read()
        #if frame is None:
	#    break
	# Кастомний код з демки пихати сюди
        data.update({"faces": fr, "success": True, "emotions":list_emotion, "affect":list_affect})
	# return a JSON response
	return jsonify(data)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 8080)))
