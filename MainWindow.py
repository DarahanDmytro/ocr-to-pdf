import cv2
from real_time_ocr import *
from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow,QApplication,QFileDialog

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        # loading the ui file with uic module
        uic.loadUi("mainWindow.ui", self)
        self.fileButton.clicked.connect(lambda: self.fileBut_clicked())
        self.analyzeButton.clicked.connect(lambda: self.fAnalyzeBut_clicked())

    def fileBut_clicked(self):
       self.imagePath=QFileDialog.getOpenFileName(self,
               'Відкрити файл','./',
               "Image files (*.jpg *.png *.jpeg)")[0]

    def fAnalyzeBut_clicked(self):
        image=cv2.imread('image', cv2.IMREAD_COLOR)
        for str in images_to_text([image]):
            self.resText.append(str)

